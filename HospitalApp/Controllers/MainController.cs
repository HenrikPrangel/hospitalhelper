﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAL.Interfaces;
using Domain;
using HospitalApp.Models;
using Microsoft.AspNetCore.Mvc;

namespace HospitalApp.Controllers
{
    public class MainController : Controller
    {
        IUnitOfWork _uow;
        InteractiveDiagnoseViewModel _vm;

        public MainController(IUnitOfWork uow, InteractiveDiagnoseViewModel vm)
        {
            _uow = uow;
            _vm = vm;
        }


        public IActionResult Index()
        {

            MainViewModel vm = new MainViewModel
            {
                AllDiseases = _uow.Diseases.All()
            };

            //Check to see that data has actually been imported at some point
            if (vm.AllDiseases.Count() == 0)
            {
                return RedirectToAction("Index", "Home");
            }

            //Get ViewModel values to show on view
            vm.DiseasesWithMostSymptoms = _uow.Diseases.GetWithMostSymptoms();
            vm.UniqueSymptomNr = _uow.Symptoms.GetUniqueSymptomNumber();
            vm.MostPopularSymptoms = _uow.Symptoms.Get3MostPopularSymptoms();

            return View(vm);
        }


        //First diagnose method that gets neccesary symptoms
        [HttpGet]
        public IActionResult Diagnose()
        {
            DiagnoseViewModel vm = new DiagnoseViewModel
            {
                AllSymptoms = SymptomDTO.Transform(_uow.Symptoms.All())
            };
            return View(vm);
        }


        //Second diagnose method that gets the possbile diseases based on previously checked symptoms
        [HttpPost]
        public IActionResult Diagnose(DiagnoseViewModel dvm)
        {
            //Check to see that some checkboxes were actually checked.
            if (dvm.Ids == null)
            {
                return RedirectToAction("Index", "Main");
            }

            //First we get all symptoms again, incase the user wants to do another search
            dvm.AllSymptoms = SymptomDTO.Transform(_uow.Symptoms.All());

            //Based on checked symptoms, we first find all "relational" objects that are tied to the chosen symptoms: Symptom - relation - Disease
            //From those objects we find all diseases related to them
            var DiseaseAndSymptomRelations = _uow.SymptomsOnDiseases.FindDiseaseBasedOnSymptom(dvm.Ids as List<int>);
            dvm.PossibleDiseases = _uow.Diseases.FindMany(DiseaseAndSymptomRelations.Select(i => i.DiseaseId).ToList());

            return View(dvm);
        }

#warning Needs refactoring
        #region Interactive diagnose

        [HttpGet]
        public IActionResult InteractiveDiagnose()
        {
            //Get and set initial ViewModel values
            _vm.HandledRelationData = _uow.SymptomsOnDiseases.All() as List<SymptomOnDisease>;
            var GroupedAndOrderedList = _vm.HandledRelationData.GroupBy(x => x.SymptomId).OrderBy(x => x.Count()).ToList();
            _vm.HandledRelationData = GroupedAndOrderedList.SelectMany(group => group).ToList();
            _vm.UserDisease = null;
            _vm.CheckedIds = new List<int>();

            return View(_vm);
        }

        [HttpPost]
        public IActionResult InteractiveDiagnose(InteractiveDiagnoseViewModel VM)
        {
            //Filter the list based on user decision
            _vm.HandledRelationData = handle(VM.UserDecision, VM.SymptomId, _vm.HandledRelationData);

            //Add the symptomId to checkList inorder to not ask the same symptom again
            _vm.CheckedIds.Add(VM.SymptomId);

            //Reorder the list, incase user decision brought about more optimal paths
            _vm.HandledRelationData.GroupBy(x => x.SymptomId).OrderBy(x => x.Count()).SelectMany(group => group).ToList();

            //Check to see whether we've found the disease
            if (_vm.HandledRelationData.GroupBy(x => x.DiseaseId).Count() == 1)
            {
                _vm.UserDisease = _vm.HandledRelationData.First().disease;
            }
            return View(_vm);
        }



        /* 
         * Needs refactoring
         */
        public List<SymptomOnDisease> handle(bool value, int id, List<SymptomOnDisease> sod)
        {
            //if true
            if (value)
            {
                //Step 1: Get all diseases that dont have the symptom so that we can remvoe them
                var toRemove = _uow.SymptomsOnDiseases.All().Where(x => x.SymptomId != id).ToList();
                var toAddBack = new List<SymptomOnDisease>();

                //Step 2: Remove all unneeded diseases
                foreach (var item in toRemove)
                {
                    //sod.Remove(item);
                    sod.RemoveAll(x => x.SymptomId == item.SymptomId && x.DiseaseId == item.DiseaseId);
                }

                //Step 3: Because needed diseasedata was also removed, we need to find it again
                foreach (var item in sod)
                {
                    toAddBack.AddRange(_uow.SymptomsOnDiseases.All().Where(x => x.DiseaseId == item.DiseaseId));
                }

                //Step 5: add back required diseasedata
                sod = toAddBack;

            }
            //if false --- Should be finished and working
            else
            {
                //Get all diseases that dont have the symptom so that we can remvoe them later
                var toRemove = _uow.SymptomsOnDiseases.All().Where(x => x.SymptomId == id).ToList() as List<SymptomOnDisease>;
                var toRemove2 = new List<SymptomOnDisease>();
                toRemove2.AddRange(toRemove);
                //Gets all additional disease relationships related to diseases we need to remove
                foreach (var item in toRemove)
                {
                    toRemove2.AddRange(_uow.SymptomsOnDiseases.All().Where(x => x.DiseaseId == item.DiseaseId));
                }

                //Remove all unneeded diseases and their relations
                foreach (var item in toRemove2)
                {
                    sod.RemoveAll(x => x.SymptomId == item.SymptomId && x.DiseaseId == item.DiseaseId);
                }
            }
            return sod;
        }
        #endregion
    }
}