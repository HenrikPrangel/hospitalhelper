﻿using DAL.Interfaces.Repositories;
using Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.EF.Repositories
{
    public class EFSymptomRepository : EFRepository<Symptom>, ISymptomRepository
    {
        public EFSymptomRepository(DbContext dbContext) : base(dbContext)
        {
        }


        /// <summary>
        /// Finds the Symptom by its name property
        /// </summary>
        /// <param name="Name">The name of the symptom</param>
        /// <returns>The symptom object, which is tied to the input name</returns>
        public async Task<Symptom> FindByName(string Name)
        {
            return await RepositoryDbSet.Where(x => x.SymptomName == Name).FirstOrDefaultAsync();
        }


        /// <summary>
        /// Gets the symptoms, that have the biggest amount of diseases related to them
        /// </summary>
        /// <returns>List containing 3 symptoms</returns>
        public IEnumerable<Symptom> Get3MostPopularSymptoms()
        {
            var ReturnList = new List<Symptom>();

            /*
             *We order the symptoms by the amount of relations (to diseases) the symptom has, 
             *and then group the symptoms by the same logic (amount of relations to diseases),
             *after which select each group indivaidually and order the elements within them alphabetically
             *Finally we take the 3 first elements
             */
            return RepositoryDbSet.OrderByDescending(x => x.Diseases.Count()).GroupBy(y => y.Diseases.Count())
                .SelectMany(g => g.OrderBy(x => x.SymptomName)).Take(3).ToList();
        }


        /// <summary>
        /// Gets the number of unique symptoms
        /// </summary>
        /// <returns>Int value of unique symptoms</returns>
        public int GetUniqueSymptomNumber()
        {
            return RepositoryDbSet.GroupBy(x => x.SymptomName).Where(x => x.Count() == 1).ToList().Count();

            //At First i thought i had to return the number of symtpoms that were unique by being attached to only 1 disease
            //return RepositoryDbSet.Where(x => x.Diseases.Count() == 1).ToList().Count();
        }
    }
}
