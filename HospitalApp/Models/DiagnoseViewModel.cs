﻿using Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HospitalApp.Models
{
    public class DiagnoseViewModel
    {
        public IEnumerable<SymptomDTO> AllSymptoms { get; set; } = new List<SymptomDTO>();

        public IEnumerable<Disease> PossibleDiseases { get; set; } = new List<Disease>();

        public List<int> Ids { get; set; }
    }

    public class SymptomDTO
    {
        public int SymptomId { get; set; }
        public String SymptomName { get; set; }
        public bool IsChecked { get; set; }


        /// <summary>
        /// Transforms Symptom objects into SymptomDTO objects
        /// </summary>
        /// <param name="list">List of Symptom objects to transform</param>
        /// <returns>List of transformed SymptomDTO objects</returns>
        public static List<SymptomDTO> Transform(IEnumerable<Symptom> list)
        {
            List<SymptomDTO> rv = new List<SymptomDTO>();
            foreach (var item in list)
            {
                rv.Add(new SymptomDTO
                {
                    SymptomId = item.SymptomId,
                    SymptomName = item.SymptomName,
                });
            }
            return rv;
        }
    }
}
