﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiseaseDatabase
{
    class Program
    {
        static void Main(string[] args)
        {
            var csvLines = File.ReadAllLines("Diseases.csv");
            

            foreach (var csvLine in csvLines)
            {

                var lineItems = csvLine.Split(',');
                foreach (var item in lineItems)
                {
                    if(item == lineItems.First())
                    {
                        continue;
                    }
                    Console.WriteLine(item);
                }
                Console.WriteLine();
            }
            Console.ReadLine();
        }
    }
}
