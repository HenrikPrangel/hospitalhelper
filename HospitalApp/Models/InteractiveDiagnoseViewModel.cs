﻿using Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HospitalApp.Models
{
    public class InteractiveDiagnoseViewModel
    {
        public List<SymptomOnDisease> HandledRelationData { get; set; }

        public List<int> CheckedIds { get; set; }

        public bool UserDecision { get; set; }

        public int SymptomId { get; set; }

        public Disease UserDisease { get; set; }
    }
}
