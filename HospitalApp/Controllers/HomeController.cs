﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using HospitalApp.Models;
using DAL.Interfaces;
using System.IO;
using Domain;

namespace HospitalApp.Controllers
{
    public class HomeController : Controller
    {
        IUnitOfWork _uow;

        public HomeController(IUnitOfWork uow)
        {
            _uow = uow;
        }


        public IActionResult Index()
        {
            return View();
        }


        public async Task<IActionResult> Main(String FileName)
        {
#warning FIX-When-Wiser Opening DB connections in foreach is not optimal

            try
            {
                //Read in cvsLines from file with the entered FileName
                var csvLines = System.IO.File.ReadAllLines(FileName);


                //Clear the DB before new data is added
                _uow.DropDBDataAsync();

                var LocalSymptomCheck = new List<String>();
                foreach (var csvLine in csvLines)
                {
                    var lineItems = csvLine.Split(',');

                    //We take the first string item, which is always the disease, and add it to the DB
                    //We also assume, that every disease is unique and only appeares once in the input document
                    var NewDisease = new Disease
                    {
                        DiseaseName = lineItems.First(),
                    };
                    await _uow.Diseases.AddAsync(NewDisease);

                    //We take the rest of the string items, which we assume are always symptoms, and add them to the DB
                    foreach (var lineItem in lineItems)
                    {
                        //First lineitem is the disease, which we dont need
                        if (lineItem == lineItems.First())
                        {
                            continue;
                        }
                        var NewSymptom = new Symptom
                        {
                            SymptomName = lineItem,
                        };
                        //Check to see, if data has already been inserted to DB - For the moment, the DB itself does not check for duplicates
                        if (!LocalSymptomCheck.Contains(lineItem))
                            await _uow.Symptoms.AddAsync(NewSymptom);

                        await _uow.SaveChangesAsync();

                        //We also add the corresponding relations between diseases and symptoms
                        var _disease = await _uow.Diseases.FindByName(NewDisease.DiseaseName);
                        var _symptom = await _uow.Symptoms.FindByName(NewSymptom.SymptomName);

                        await _uow.SymptomsOnDiseases.AddAsync(new SymptomOnDisease
                        {
                            disease = _disease,
                            symptom = _symptom,
                        });

                        //We add the symptom to the local check to insure no duplicate symptoms are added to the DB
                        LocalSymptomCheck.Add(lineItem);
                    }
                    await _uow.SaveChangesAsync();
                }

                return RedirectToAction("Index", "Main");

            }
            catch (Exception e)
            {
                throw new FileNotFoundException($"Error: File name error");
            }

        }


        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
