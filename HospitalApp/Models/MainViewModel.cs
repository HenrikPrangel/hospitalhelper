﻿using Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HospitalApp.Models
{
    public class MainViewModel
    {
        public IEnumerable<Disease> AllDiseases { get; set; }

        public IEnumerable<Disease> DiseasesWithMostSymptoms { get; set; }

        public IEnumerable<Symptom> MostPopularSymptoms { get; set; }

        public int UniqueSymptomNr { get; set; }

    }
}
