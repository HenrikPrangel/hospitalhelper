﻿using DAL.Interfaces;
using Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.EF
{
    public class ApplicationDBContext : DbContext, IDataContext
    {
        public DbSet<Domain.Disease> Diseases { get; set; }
        public DbSet<Domain.Symptom> Symptoms { get; set; }
        public DbSet<Domain.SymptomOnDisease> SymptomsOnDiseases { get; set; }
        public ApplicationDBContext(DbContextOptions<ApplicationDBContext> options)
            : base(options)
        {
        }
    }
}
