﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Interfaces
{
    public interface IRepository<TEntity> where TEntity : class
    {
        /// <summary>
        /// Gets all Entities>
        /// </summary>
        /// <returns>Enumerable of Entity - objects </returns>
        IEnumerable<TEntity> All();

        /// <summary>
        /// Gets all Entities Asynchronously
        /// </summary>
        /// <returns>Enumerable of Entity - objects </returns>
        Task<IEnumerable<TEntity>> AllAsync();

        /// <summary>
        /// Adds Entity into the DB
        /// </summary>
        /// <param name="entity">The Entity to be added to the DB</param>
        void Add(TEntity entity);


        /// <summary>
        /// Adds Entity into the DB Asynchronously
        /// </summary>
        /// <param name="entity">The Entity to be added to the DB</param>
        Task AddAsync(TEntity entity);
    }
}
