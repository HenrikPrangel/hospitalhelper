﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Domain
{
    public class Disease
    {
        public int DiseaseId { get; set; }

        [Required]
        [MaxLength(250)]
        public String DiseaseName { get; set; }

        public List<SymptomOnDisease> Symptoms { get; set; }

    }
}
