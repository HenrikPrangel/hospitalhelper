﻿using DAL.EF.Repositories;
using DAL.Interfaces;
using DAL.Interfaces.Repositories;
using Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.EF
{
    public class AppUnitOfWork : IUnitOfWork
    {
        private readonly ApplicationDBContext _dbContext;

        public AppUnitOfWork(IDataContext dbContext)
        {
            _dbContext = dbContext as ApplicationDBContext;
            if (_dbContext == null)
            {
                throw new ArgumentNullException(nameof(dbContext));
            }
        }

        public IDiseaseRepository Diseases => new EFDiseaseRepository(_dbContext as ApplicationDBContext);

        public ISymptomRepository Symptoms => new EFSymptomRepository(_dbContext as ApplicationDBContext);

        public ISymptomOnDiseaseRepository SymptomsOnDiseases => new EFSymptomOnDiseaseRepository(_dbContext as ApplicationDBContext);


        /// <summary>
        /// Gets all data from all tables in the DB and then deletes it
        /// </summary>
        public void DropDBDataAsync()
        {

#warning FIX-When-Wiser- Probably very crude and resource heavy, but at the moment it works.

            var Diseases = _dbContext.Diseases.ToList();
            var Symptoms = _dbContext.Symptoms.ToList();
            var SymptomsOnDiseases = _dbContext.SymptomsOnDiseases.ToList();

            _dbContext.RemoveRange(SymptomsOnDiseases);
            _dbContext.RemoveRange(Symptoms);
            _dbContext.RemoveRange(Diseases);
            _dbContext.SaveChanges();
        }

        public void SaveChanges()
        {
            _dbContext.SaveChanges();
        }

        public async Task SaveChangesAsync()
        {
            await _dbContext.SaveChangesAsync();
        }
    }
}
