﻿using DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.EF
{
    public class EFRepository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        protected DbContext RepositoryDbContext;
        protected DbSet<TEntity> RepositoryDbSet;

        public EFRepository(DbContext dbContext)
        {
            RepositoryDbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
            RepositoryDbSet = RepositoryDbContext.Set<TEntity>();
        }


        /// <summary>
        /// Gets all Entities>
        /// </summary>
        /// <returns>Enumerable of Entity - objects </returns>
        public virtual IEnumerable<TEntity> All()
        {
            return RepositoryDbSet.ToList();
        }


        /// <summary>
        /// Gets all Entities Asynchronously
        /// </summary>
        /// <returns>Enumerable of Entity - objects </returns>
        public virtual async Task<IEnumerable<TEntity>> AllAsync()
        {
            return await RepositoryDbSet.ToListAsync();
        }


        /// <summary>
        /// Adds Entity into the DB
        /// </summary>
        /// <param name="entity">The Entity to be added to the DB</param>
        public void Add(TEntity entity)
        {
            if (!RepositoryDbSet.Contains(entity))
            {
                RepositoryDbSet.Add(entity);
            }
            else
                throw new NotImplementedException();
        }


        /// <summary>
        /// Adds Entity into the DB Asynchronously
        /// </summary>
        /// <param name="entity">The Entity to be added to the DB</param>
        public async Task AddAsync(TEntity entity)
        {
            if (!RepositoryDbSet.Contains(entity))
            {
                await RepositoryDbSet.AddAsync(entity);
            }
            else
                throw new NotImplementedException();
        }
    }
}
