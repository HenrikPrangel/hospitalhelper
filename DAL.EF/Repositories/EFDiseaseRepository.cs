﻿using DAL.Interfaces.Repositories;
using Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.EF.Repositories
{
    public class EFDiseaseRepository : EFRepository<Disease>, IDiseaseRepository
    {
        public EFDiseaseRepository(DbContext dbContext) : base(dbContext)
        {
        }


        /// <summary>
        /// Gets all Disease objects in the DB
        /// </summary>
        /// <returns>Enumerable of all Disease objects</returns>
        public override IEnumerable<Disease> All()
        {
            return RepositoryDbSet.Include(i => i.Symptoms).ThenInclude(a => a.symptom).ToList();
        }


        /// <summary>
        /// Gets a disease by its name
        /// </summary>
        /// <param name="name">The name property of the disease to search for</param>
        /// <returns>Disease object that has the input name</returns>
        public async Task<Disease> FindByName(string name)
        {
            return await RepositoryDbSet.Where(x => x.DiseaseName == name).FirstOrDefaultAsync();
        }


        /// <summary>
        /// Finds and gets many Disease objects
        /// </summary>
        /// <param name="ids">List of disease Id-s based on which the corresponding disease objects will be searched for</param>
        /// <returns>List of diseases, that had a matching id</returns>
        public List<Disease> FindMany(List<int> ids)
        {
            return RepositoryDbSet.Include(i => i.Symptoms).ThenInclude(a => a.symptom).Where(x => ids.Contains(x.DiseaseId)).ToList();
        }


        /// <summary>
        /// Gets 3 Diseases that have the most symptoms
        /// </summary>
        /// <returns>Enumerable of Diseases</returns>
        public IEnumerable<Disease> GetWithMostSymptoms()
        {

            /*
             *We order the diseases by the amount of relations (to symptoms) the disease has, 
             *and then group the diseases by the same logic (amount of relations to symptoms),
             *after which we select all the groups individually and sort them alphabetically
             *Finaly we only return the 3 first elements.
             */
            return RepositoryDbSet.OrderByDescending(x => x.Symptoms.Count()).GroupBy(y => y.Symptoms.Count())
                .SelectMany(g => g.OrderBy(x => x.DiseaseName)).Take(3).ToList();
        }
    }
}
