﻿using Domain;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Interfaces.Repositories
{
    public interface ISymptomRepository : IRepository<Symptom>
    {
        /// <summary>
        /// Finds the Symptom by its name property
        /// </summary>
        /// <param name="Name">The name of the symptom</param>
        /// <returns>The symptom object, which is tied to the input name</returns>
        Task<Symptom> FindByName(String Name);

        /// <summary>
        /// Gets the number of unique symptoms
        /// </summary>
        /// <returns>Int value of unique symptoms</returns>
        int GetUniqueSymptomNumber();

        /// <summary>
        /// Gets the symptoms, that have the biggest amount of diseases related to them
        /// </summary>
        /// <returns>List containing 3 symptoms</returns>
        IEnumerable<Symptom> Get3MostPopularSymptoms();
    }
}
