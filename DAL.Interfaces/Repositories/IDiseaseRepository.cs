﻿using Domain;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Interfaces.Repositories
{
    public interface IDiseaseRepository : IRepository<Disease>
    {
        /// <summary>
        /// Gets a disease by its name
        /// </summary>
        /// <param name="name">The name property of the disease to search for</param>
        /// <returns>Disease object that has the input name</returns>
        Task<Disease> FindByName(String name);


        /// <summary>
        /// Finds and gets many Disease objects
        /// </summary>
        /// <param name="ids">List of disease Id-s based on which the corresponding disease objects will be searched for</param>
        /// <returns>List of diseases, that had a matching id</returns>
        List<Disease> FindMany(List<int> id);


        /// <summary>
        /// Gets 3 Diseases that have the most symptoms
        /// </summary>
        /// <returns>Enumerable of Diseases</returns>
        IEnumerable<Disease> GetWithMostSymptoms();

    }
}
