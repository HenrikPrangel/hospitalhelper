﻿using Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Interfaces.Repositories
{
    public interface ISymptomOnDiseaseRepository :IRepository<SymptomOnDisease>
    {

        /// <summary>
        /// Finds all SymptomOnDisease "relation-class" objects that are related to the input id of a symptom
        /// </summary>
        /// <param name="id">The id of the symptom for which relations are searched for</param>
        /// <returns>IEnumerable of SymptomOnDisease "relation-class" objects </returns>
        IEnumerable<SymptomOnDisease> FindDiseaseBasedOnSymptom(List<int> id);
    }
}
