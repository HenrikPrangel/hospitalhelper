﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain
{
    public class SymptomOnDisease
    {
        public int SymptomOnDiseaseId { get; set; }

        public int DiseaseId { get; set; }
        public Disease disease { get; set; }

        public int SymptomId { get; set; }
        public Symptom symptom { get; set; }

    }
}
