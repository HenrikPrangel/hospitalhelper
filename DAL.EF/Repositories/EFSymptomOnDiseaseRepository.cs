﻿using DAL.Interfaces.Repositories;
using Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.EF.Repositories
{
    public class EFSymptomOnDiseaseRepository : EFRepository<SymptomOnDisease>, ISymptomOnDiseaseRepository
    {
        public EFSymptomOnDiseaseRepository(DbContext dbContext) : base(dbContext)
        {
        }


        /// <summary>
        /// Gets all SymptomOnDisease objects in the DB
        /// </summary>
        /// <returns>Enumerable containing all SymptomOnDisease-s</returns>
        public override IEnumerable<SymptomOnDisease> All()
        {
            return RepositoryDbSet.Include(i => i.symptom).Include(i => i.disease).ToList();
        }


        /// <summary>
        /// Finds all SymptomOnDisease "relation-class" objects that are related to the input id of a symptom
        /// </summary>
        /// <param name="ids">The id of the symptom for which relations are searched for</param>
        /// <returns>IEnumerable of SymptomOnDisease "relation-class" objects </returns>
        public IEnumerable<SymptomOnDisease> FindDiseaseBasedOnSymptom(List<int> ids)
        {

#warning FIX- Needs refactoring

            var returnList = new List<SymptomOnDisease>();
            var TempList = new List<SymptomOnDisease>();

            returnList.AddRange(RepositoryDbSet.Where(x => ids.Contains(x.SymptomId)).ToList());

            /*Double check, that all symptoms are present on diseases that were found,
             * if not, then add it to a temporary list
             * -To insure that only diseases that have all the input symptoms are displayed
             */
            foreach (var item in returnList)
            {
                foreach (var ID in ids)
                {
                    if (!RepositoryDbSet.Where(x => x.DiseaseId == item.DiseaseId && x.SymptomId == ID).Any())
                    {
                        TempList.Add(item);
                    }
                }
            }

            //Remove all incorrect diseases from returnList
            foreach (var item in TempList)
            {
                returnList.Remove(item);
            }
            return returnList;
        }
    }
}
