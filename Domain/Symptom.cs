﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Domain
{
    public class Symptom
    {
        public int SymptomId { get; set; }

        [Required]
        [MaxLength(250)]
        public String SymptomName { get; set; }

        public List<SymptomOnDisease> Diseases { get; set; }

    }
}
