﻿using DAL.Interfaces.Repositories;
using Domain;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Interfaces
{
    public interface IUnitOfWork
    {

        /// <summary>
        /// Saves changes to DB
        /// </summary>
        void SaveChanges();

        /// <summary>
        /// Saves changes to DB asynchronously
        /// </summary>
        /// <returns></returns>
        Task SaveChangesAsync();

        /// <summary>
        /// Gets all data from all tables in the DB and then deletes it
        /// </summary>
        void DropDBDataAsync();

        IDiseaseRepository Diseases { get; }
        ISymptomRepository Symptoms { get; }
        ISymptomOnDiseaseRepository SymptomsOnDiseases { get; }
    }
}
